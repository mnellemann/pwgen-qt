import QtQuick 2.12
import QtQuick.Window 2.12
import QtQuick.Controls 2.15
import QtQuick.Layouts 1.11
//import "libpw.js" as Logic
import biz.nellemann.libpassword 1.0;

ApplicationWindow {
    id: root
    width: 320
    height: 440
    visible: true
    title: qsTr("Password Generator")

    property string password: ""
    onPasswordChanged: {
        console.log("password : "+ password);
    }

    property int length: spinBoxLength.value;
    property bool useCapitalized: checkBoxCapitalize.checkState;
    property bool useNumerals: checkBoxNumerals.checkState;
    property bool useSymbols: checkBoxSymbols.checkState;

    LibPassword {
        id: libpassword
    }

    /* This should only be visible on mobile, not desktop - but how ?
    header: ToolBar {
        RowLayout {
            anchors.fill: parent
            Label {
                text: "Password Generator"
                elide: Label.ElideRight
                horizontalAlignment: Qt.AlignHCenter
                verticalAlignment: Qt.AlignVCenter
                Layout.fillWidth: true
            }
        }
    }*/


    Grid {
        id: grid
        width: root.width - 30
        //anchors.fill: parent
        padding: 15
        spacing: 15
        rows: 0
        columns: 1

        GroupBox {
            id: groupBox1
            width: grid.width
            title: qsTr("Options")

            ColumnLayout {
                anchors.fill: parent

                CheckBox {
                    id: checkBoxCapitalize
                    text: qsTr("Capitalize")

                }

                CheckBox {
                    id: checkBoxNumerals
                    text: qsTr("Numerals")
                }

                CheckBox {
                    id: checkBoxSymbols
                    text: qsTr("Symbols")
                }
            }

        }

        GroupBox {
            id: groupBox2
            width: grid.width
            title: qsTr("Length")

            ColumnLayout {
                anchors.fill: parent

                SpinBox {
                    id: spinBoxLength
                    Layout.fillWidth: true
                    editable: true
                    value: 16
                    from: 4
                    to: 64
                }
            }
        }

        RowLayout {
            id: rowLayout1
            width: grid.width
            height: 55

            Button {
                id: buttonGenerate
                width: rowLayout1.width
                text: qsTr("&Generate")
                Layout.fillWidth: true
                transformOrigin: Item.Center
                //onClicked: password = Logic.randomNumber()
                onClicked: password = libpassword.randomString(length, useCapitalized, useNumerals, useSymbols);
            }
        }

        RowLayout {
            id: rowLayout2
            width: grid.width
            height: 55

            TextField {
                id: textField
                width: 220
                text: password
                font.family: 'Monospace'
                font.pointSize: 12
                font.styleName: 'QFont::Fixed'
                font.hintingPreference: Font.PreferVerticalHinting
                renderType: Text.NativeRendering

                Layout.fillWidth: true
                readOnly: true
            }

            Button {
                id: buttonCopy
                x: 230
                width: 55
                text: qsTr("&Copy")
                icon.name: "edit-copy"
                //display: AbstractButton.IconOnly
                transformOrigin: Item.Right
                onClicked: shortcut.activated()
            }

        }
    }


    // Workaround with hidden TextEdit for copy to clipboard
    TextEdit{
       id: textEdit
       visible: false
    }
    Shortcut {
        id: shortcut
        sequence: StandardKey.Copy
        onActivated: {
            textEdit.text = password
            textEdit.selectAll()
            textEdit.copy()
        }
    }

}


