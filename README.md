# Random Password Generator

This is a simple QT application you can use to generate secure random passwords.

![Screenshot](https://bitbucket.org/mnellemann/pwgen/downloads/pwgen.gif)


## License Information

* The application is [Apache 2.0](https://www.apache.org/licenses/LICENSE-2.0) licensed and free to use in any way.