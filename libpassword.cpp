#include <math.h>
#include "libpassword.h"

LibPassword::LibPassword(QObject *parent) : QObject(parent)
{
    srand (time(NULL));
}


QString LibPassword::randomString(int length, bool useCapitalized, bool useNumerals, bool useSymbols)
{
    int charsRemaining = length;
    int numberOfCharsCapitalized= 0;
    int numberOfCharsNumerals = 0;
    int numberOfCharsSymbols = 0;

    QVector<int> *charTypeVector = new QVector<int>;

    if(useCapitalized) {
        numberOfCharsCapitalized = floor(length * 0.30);    // We use 30% of length for uppercase alpha
        charsRemaining = charsRemaining - numberOfCharsCapitalized;
        addCharTypeToVector(charTypeVector, numberOfCharsCapitalized, CharType::CAPS);
    }

    if(useNumerals) {
        numberOfCharsNumerals = floor(length * 0.25);   // We use 25% of length for Numerals
        charsRemaining = charsRemaining - numberOfCharsNumerals;
        addCharTypeToVector(charTypeVector, numberOfCharsNumerals, CharType::NUMS);
    }

    if(useSymbols) {
        numberOfCharsSymbols = floor(length * 0.20);    // We use 20% of length for Symbols
        charsRemaining = charsRemaining - numberOfCharsSymbols;
        addCharTypeToVector(charTypeVector, numberOfCharsSymbols, CharType::SYMS);
    }

    // We use remains length for lowercase alpha
    addCharTypeToVector(charTypeVector, charsRemaining, CharType::NORM);

    // Shuffle the vector of int's
    shuffle(charTypeVector);

    QString str = *new QString();
    for (int i = 0; i < charTypeVector->size(); i++) {

        switch (charTypeVector->at(i)) {
            case NORM:
                str.append(getRandomChar(strNorm));
                break;
            case CAPS:
                str.append(getRandomChar(strCaps));
                break;
            case NUMS:
                str.append(getRandomChar(strNums));
                break;
            case SYMS:
                str.append(getRandomChar(strSyms));
                break;
        }
    }

    return str;
}


QChar LibPassword::getRandomChar(const QString str) {
    int randInt = rand() % str.length();
    const QChar c = str.at(randInt);
    return c;
}


void LibPassword::addCharTypeToVector(QVector<int> *list,  int numCharsToAdd, CharType type)
{
    for(int i = 0; i < numCharsToAdd; i++) {
        list->append(type);
    }
}


void LibPassword::shuffle(QVector<int> *v)
{
    // Start from the last element and swap
    // one by one. We don't need to run for
    // the first element that's why i > 0
    for (int i = v->length() - 1; i > 0; i--)
    {
        // Pick a random index from 0 to i
        int j = rand() % (i + 1);

        // Swap arr[i] with the element
        // at random index
        v->swapItemsAt(i, j);
    }
}
