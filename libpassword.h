#ifndef LIBPASSWORD_H
#define LIBPASSWORD_H

#include <QObject>
#include <QString>

class LibPassword : public QObject
{
    Q_OBJECT
public:
    explicit LibPassword(QObject *parent = nullptr);
    Q_INVOKABLE QString randomString(int, bool, bool, bool);

private:

    enum CharType {
        NORM = 1,
        CAPS = 2,
        NUMS = 3,
        SYMS = 4
    };

    const QString strNorm = "abcdefghijklmnopqrstuvxyz";
    const QString strCaps = "ABCDEFGHIJKLMNOPQRSTUVXYZ";
    const QString strNums = "0123456789";
    const QString strSyms = "!\"'#%&/\\()=?_-+*,.<>";
    QChar getRandomChar(const QString);
    void addCharTypeToVector(QVector<int>*, int, CharType);
    void shuffle(QVector<int> *v);


signals:

};

#endif // LIBPASSWORD_H
